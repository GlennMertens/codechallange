package be.glennmertens.calculator;

public class Calculator {
	private static char[] array;
	private static int index;

	/*
	 * TO-DO - check for rong input - Order of Operations
	 */

	public static double calculate(String calculation) {
		index = -1;
		array = calculation.toCharArray();
		float result = 0;
		result = nextChar();
		while (index < array.length - 1) {
			switch (nextChar()) {
			case '-':
				result -= nextChar();
				break;
			case '*':
				result *= nextChar();
				break;
			case '/':
				result /= nextChar();
				break;
			case '+':
				result += nextChar();
				break;
			}
		}
		return result;
	}

	private static boolean isMath(char c) {
		switch (c) {
		case '-':
			return true;
		case '*':
			return true;
		case '/':
			return true;
		case '+':
			return true;
		default:
			return false;
		}
	}

	private static int nextChar() {
		index++;
		if (array[index] != ' ') {
			if (isMath(array[index])) {
				return array[index];
			} else {
				return Character.getNumericValue(array[index]);
			}
		} else {
			nextChar();
			return -1;
		}

	}

}
