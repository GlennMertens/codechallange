package be.glennmertens.calculator;

import static org.junit.Assert.*;

import static be.glennmertens.calculator.Calculator.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	private Calculator calc;

	@Before
	public void init() {
		calc = new Calculator();
	}

	@Test
	public void BasicTest() {
		assertEquals(calculate("5+6"), 11.0, 0.01);
		assertEquals(calculate("5+6+5"), 16.0, 0.01);
		assertEquals(calculate("5+5/2"), 5.0, 0.01);
		assertEquals(calculate("2-5"), -3.0, 0.01);
	}

}
